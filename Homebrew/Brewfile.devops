# vi: filetype=brewfile

# brew "ansible" "ansible-lint"

## Clouds
brew "checkov"
# multi-cloud asset lister, https://github.com/projectdiscovery/cloudlist
brew "cloudlist"
# AWS
brew "awscli" "s3cmd"
brew "aws-nuke"
# brew "cloud-nuke"
# Digital Ocean
# brew "doctl"

# Oracle Cloud Infrastructure
# brew "oci-cli"

# Microsoft Azure

# Google Cloud Platform

# Red Hat OpenShift Cluster Manager cli
# brew "ocm"

# Other
# brew "linode-cli"
# brew "circleci"
# CLI for local GitHub Actions
# brew "act"
## Manage applications and infrastructure on Massdriver Cloud, https://www.massdriver.cloud/
# brew "massdriver"
## End Clouds

## Terraform & friends
brew "terraform" "terraform-docs" "tflint" "terraform_landscape" "tfk8s" "terrascan" "tf-profile"
brew "terragrunt"
# Terraform version/environment management, https://github.com/tfutils/tfenv
# brew "tfenv"
# Scaffolding for TF-managed AWS, https://github.com/tfutils/tfscaffold
# brew "tfscaffold"
# Interactive Terraform Visualizer, https://github.com/im2nguyen/rover
brew "terraform-rover"
# CLI to beautify `terraform graph` output, https://github.com/pcasteran/terraform-graph-beautifier
brew "terraform-graph-beautifier"

## Containers
cask "docker" if OS.mac?
brew "docker" "docker-compose" "hadolint"
# docker-compose-completion has been disabled because it no upstream support for v2
# brew "docker-compose-completion"

# brew "podman"
# brew "podman-compose"
# cask "podman-desktop" if OS.mac?

# crane is a tool for interacting with remote images and registries. https://github.com/google/go-containerregistry#crane
brew "crane"

brew "lazydocker"
# ContaiNERD CTL - Docker-compatible CLI for containerd https://github.com/containerd/nerdctl
# brew "nerdctl"
# lima, Linux virtual machines, typically on macOS, for running containerd https://github.com/lima-vm/lima
# brew "lima"
# colima, Container runtimes on macOS (and Linux) with minimal setup https://github.com/abiosoft/colima
# brew "colima"

# brew "nomad"

## Kubernetes
brew "kubernetes-cli" "kustomize"
## Manage complex enterprise Kubernetes environments as code, https://github.com/kubecfg/kubecfg
brew "kubecfg"
## KubeConfig Managers, https://github.com/ahmetb/kubectx, https://kubecm.cloud
brew "kubectx" "kubecm"
# KUbernetes Test TooL, https://kuttl.dev
brew "kuttl"
# check your clusters for use of deprecated APIs, https://github.com/doitintl/kube-no-trouble
brew "kubent"
# CLI tool to discover unused Kubernetes resources, https://github.com/yonahd/kor
brew "kor"
# Vulnerability scanner for container images and filesystems, https://github.com/anchore/grype
brew "grype"
# Periodically kills random pods in your Kubernetes cluster, https://github.com/linki/chaoskube
# brew "chaoskube"

# brew "helm"
# brew "skaffold"

# pick one (or more)?
# brew "kind"
brew "k9s"
brew "k3sup"
# brew "minikube"

# Spin up fresh cloud development environments: https://www.gitpod.io/

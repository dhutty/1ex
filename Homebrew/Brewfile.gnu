# vi: filetype=brewfile
# brewfile for installing GNU tools because macOS-installed versions are typically old
# You probably want to do some PATH manipulation like:
# echo 'PATH="/opt/homebrew/opt/gawk/libexec/gnubin:$PATH"' >> ~/.zshrc
# echo 'PATH="/opt/homebrew/opt/gnu-sed/libexec/gnubin:$PATH"' >> ~/.zshrc
# echo 'PATH="/opt/homebrew/opt/gnu-tar/libexec/gnubin:$PATH"' >> ~/.zshrc
# echo 'PATH="/opt/homebrew/opt/coreutils/libexec/gnubin:$PATH"' >> ~/.zshrc
# echo 'PATH="/opt/homebrew/opt/make/libexec/gnubin:$PATH"' >> ~/.zshrc
brew "binutils"
brew "coreutils"
brew "diffutils"
brew "ed"
brew "findutils"
brew "gawk"
brew "gnu-indent"
brew "gnu-sed"
brew "gnu-tar"
brew "less"
brew "make"

# vi: filetype=brewfile
# See also https://github.com/joelparkerhenderson/brewfile/blob/main/Brewfile
# Communications
cask "signal" if OS.mac?
brew "signal-cli"

# Multimedia tools
cask "spotify" if OS.mac?
# mini graphical Spotify player, https://www.lofi.rocks
# cask "lofi" if OS.mac?
# spotmenu for Spotify, https://github.com/kmikiy/SpotMenu
cask "spotmenu" if OS.mac?
# Cross-platform ncurses Spotify client written in Rust, https://github.com/hrkfdn/ncspot
# brew "ncspot"
cask "skim" if OS.mac?
# graphical epub reader (use Foliate on linux?)
cask "thorium" if OS.mac?
# media player: https://iina.io
# cask "iina"
cask "vlc" if OS.mac?
cask "xld" if OS.mac?
brew "yt-dlp"
# text-to-speech, https://www.ah-soft.com/voice/
# cask "voicepeak"
# Optimize and resize images, https://git.sr.ht/~jamesponddotco/imgdiet-go
brew "imgdiet"

## Mail tools
# text email client, https://aerc-mail.org/
# https://man.sr.ht/~rjarry/aerc/integrations/index.md
brew "aerc" "offlineimap"
brew "mairix"
# mail indexer, https://notmuch.readthedocs.io
brew "notmuch"
# Synchronize calendars and contacts https://github.com/pimutils/vdirsyncer
brew "vdirsyncer"

# Publishing
# pdf rendering
# brew "poppler"
# CLI epub reader, https://github.com/wustho/epr
brew "epr"
# Presentations/Slides
# for the Markdown Presentation Ecosystem, https://github.com/marp-team/marp
# https://github.com/marp-team/marp-cli
# brew "marp-cli"

# FOSS scientific/technical publishing system, https://quarto.org/
# cask "quarto" if OS.mac?
# Asciidoc Editor and Toolchain, https://asciidocfx.com/ https://github.com/asciidocfx/AsciidocFX
# cask "asciidocfx" if OS.mac?

# brew "ipfs"
## Tor
# tor
# torsocks

# bittorrent/transmission?

## Other
# HN TUI: https://github.com/bensadeh/circumflex
brew "circumflex"
# Terminal file explorer https://github.com/mgunyho/tere
# brew "tere"
# Terminal file manager https://github.com/antonmedv/llama
# brew "llama"

# https://github.com/schachmat/wego weather tui
brew "wego"
# cli note-taking (installs a bunch of [recommended but optional](https://github.com/xwmx/nb#optional) dependencies), https://xwmx.github.io/nb
brew "nb"
brew "vifm"
# Minecraft package manager and launcher https://github.com/jakobkmar/pacmc
# brew "pacmc"

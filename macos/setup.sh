#!/bin/sh

## Preferences

#### CLI approach to change macOS user preferences
# Domains are objects that contain settings for a particular system component, installed application or a configuration .plist file
# View the Domains you can play with:
# defaults domains | tr ',' '\n'
# Basically it's going to be:
# defaults read <domain>                ## discover the name of the key to configure
# defaults read-type <domain> <key>     ## discover the Type of that Key
# defaults write <domain> <key> -<type> <new_value>     ## Set a new value for the Key
####

# `defaults read` will output all the keys' values for all domains
# therefore you can dump to a file, change a setting in the GUI, dump the after and `diff before after`

# As of Ventura, 'Finder Settings' no longer provides a graphical setting to show Hidden/dot files (although you can use a keyboard shortcut `command+shift+.`)
defaults write com.apple.finder AppleShowAllFiles -string TRUE

## software via brew-file?

## fetch dotfiles from git(hub|lab)
